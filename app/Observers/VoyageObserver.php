<?php

namespace App\Observers;

use App\Models\Voyage;

class VoyageObserver
{
    public function saving(Voyage $voyage)
    {
        $this->calculateCode($voyage);
        $this->calculateProfit($voyage);
    }

    protected function calculateProfit(Voyage $voyage)
    {
        // If is going to be submitted fill the profit. Applied on creating and updating a voyage.
        // Also calculate if revenues or expenses are changed.
        if (
            ($voyage->isDirty('status') && $voyage->status == 'submitted')
            || ($voyage->isDirty('revenues') || $voyage->isDirty('expenses'))
        ) {
            $voyage->profit = $voyage->calculateProfit();
        }
    }

    protected function calculateCode(Voyage $voyage)
    {
        // Calculate the code on creation and recalculate it every time the 'start' changes.
        if ($voyage->isDirty('start')) {
            $voyage->code = $voyage->calculateCode();
        }
    }
}
