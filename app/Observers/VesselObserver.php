<?php

namespace App\Observers;

use App\Models\Vessel;
use App\Models\Voyage;

class VesselObserver
{
    public function saving(Vessel $vessel)
    {
        if ($vessel->isDirty('name')) {
            $vessel->voyages->each(function (Voyage $voyage){
                $voyage->code = $voyage->calculateCode();
                $voyage->save();
            });
        }
    }

}
