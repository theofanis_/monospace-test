<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreVesselOpexRequest;
use App\Http\Resources\VesselOpexResource;
use App\Models\Vessel;

class VesselOpexController extends Controller
{

    public function store(StoreVesselOpexRequest $request, Vessel $vessel)
    {
        return new VesselOpexResource(
            $vessel->operational_expenses()->create(
                $request->validated()
            )
        );
    }
}
