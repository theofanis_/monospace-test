<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreVoyageRequest;
use App\Http\Requests\UpdateVoyageRequest;
use App\Http\Resources\VoyageResource;
use App\Models\Voyage;

class VoyageController extends Controller
{

    public function index()
    {
        return VoyageResource::collection(Voyage::all());
    }

    public function store(StoreVoyageRequest $request)
    {
        return Voyage::create($request->validated());
    }

    public function show(Voyage $voyage)
    {
        return new VoyageResource($voyage);
    }

    public function update(UpdateVoyageRequest $request, Voyage $voyage)
    {
        $voyage->update($request->validated());
        return new VoyageResource($voyage->refresh());
    }

}
