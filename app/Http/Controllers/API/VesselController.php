<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\Reports\VesselFinancialReportResource;
use App\Models\Vessel;

class VesselController extends Controller
{
    public function financialReport(Vessel $vessel)
    {
        return new VesselFinancialReportResource($vessel);
    }
}
