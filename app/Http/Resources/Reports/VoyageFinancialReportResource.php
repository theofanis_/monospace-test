<?php

namespace App\Http\Resources\Reports;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\Voyage
 */
class VoyageFinancialReportResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'voyage_id'                   => $this->id,
            'start'                       => $this->start->toDateTimeString(),
            'end'                         => optional($this->end)->toDateTimeString(), // last voyage may have unset end
            'voyage_revenues'             => $this->revenues,
            'voyage_expenses'             => $this->expenses,
            'voyage_profit'               => $this->profit,
            'voyage_profit_daily_average' => $this->daily_average_profit,
            'vessel_expenses_total'       => $this->total_expenses,
            'net_profit'                  => $net_profit = $this->net_profit,
            'net_profit_daily_average'    => $this->getDailyAverageNetProfitAttribute($net_profit),
//            'net_profit_daily_average'    => $this->daily_average_net_profit,
        ];
    }
}
