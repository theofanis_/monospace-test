<?php

namespace App\Http\Resources\Reports;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\Vessel
 */
class VesselFinancialReportResource extends JsonResource
{
    public function toArray($request)
    {
        //TODO paginate voyages
        return VoyageFinancialReportResource::collection($this->voyages);
    }
}
