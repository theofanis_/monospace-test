<?php

namespace App\Http\Requests;

use App\Models\Voyage;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateVoyageRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;

    public Voyage $voyage;

    protected function prepareForValidation()
    {
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->voyage = $this->route('voyage');
    }

    public function authorize()
    {
        // for challenge project Auth::user() will be null but VoyagePolicy has been built accordingly
        return $this->voyage && Gate::forUser(Auth::user())->allows('update', $this->voyage);
    }

    public function rules()
    {
        return [
            'start'    => 'required|date',
            'end'      => 'nullable|date|after:start',
            'revenues' => 'nullable|numeric',
            'expenses' => 'nullable|numeric',
            'status'   => ['required|string', Rule::in(Voyage::STATUSES)],
        ];
    }

    /**
     * @param \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->multipleOngoingVoyages()) {
                $validator->errors()->add('status', __('Vessel already has ongoing voyage.'));
            }
        });
    }

    protected function multipleOngoingVoyages()
    {
        // for status ongoing query db for existing voyages with status ongoing
        if ($this->input('status') == 'ongoing') {
            return Voyage::ongoing()->where(['vessel_id', $this->voyage->vessel_id])->exists();
            //return $this->voyage->vessel->voyages()->scopes('ongoing')->exists();
        }
        return false;
    }
}
