<?php

namespace App\Http\Requests;

use App\Models\Vessel;
use Illuminate\Foundation\Http\FormRequest;

class StoreVesselOpexRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;

    public Vessel $vessel;

    protected function prepareForValidation()
    {
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->vessel = $this->route('vessel');
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'date'      => 'required|date',
            'expenses'  => 'required|numeric',
        ];
    }

    /**
     * @param \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->multipleOpexForDate()) {
                $validator->errors()->add('date', __("Operational expenses already submitted for date :date.", ['date' => $this->input('date')]));
            }
        });
    }

    protected function multipleOpexForDate()
    {
        // query db for existing opex for same date
        return $this->vessel->operational_expenses()->where('date', $this->input('date'))->exists();
    }

}
