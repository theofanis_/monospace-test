<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreVoyageRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'vessel_id' => 'required|exists:vessels,id',
            'start'     => 'required|date',
            'end'       => 'nullable|date|after:start',
            'revenues'  => 'nullable|numeric',
            'expenses'  => 'nullable|numeric',
        ];
    }
}
