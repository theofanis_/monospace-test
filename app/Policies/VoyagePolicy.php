<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Voyage;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class VoyagePolicy
{
    use HandlesAuthorization;

    public function update(?User $user, Voyage $voyage)
    {
        if ($voyage->status == 'submitted') {
            return Response::deny(__('Voyage has been submitted'));
        }
        return true;
    }

}
