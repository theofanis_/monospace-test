<?php

namespace App\Providers;

use App\Models\Voyage;
use App\Policies\VoyagePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        Voyage::class => VoyagePolicy::class,
    ];

    public function boot()
    {
        $this->registerPolicies();
    }
}
