<?php

namespace App\Providers;

use App\Models\Vessel;
use App\Models\Voyage;
use App\Observers\VesselObserver;
use App\Observers\VoyageObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    public function boot()
    {
        Vessel::observe(VesselObserver::class);
        Voyage::observe(VoyageObserver::class);
    }
}
