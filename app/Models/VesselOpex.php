<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VesselOpex
 *
 * @property-read \App\Models\Vessel $vessel
 * @method static \Illuminate\Database\Eloquent\Builder|VesselOpex newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VesselOpex newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VesselOpex query()
 * @mixin \Eloquent
 */
class VesselOpex extends Model
{
    protected $table = 'vessel_opex';

    protected $fillable = ['date', 'expenses'];

    protected $casts = [
        'date' => 'date',
    ];

    public function vessel()
    {
        return $this->belongsTo(Vessel::class);
    }
}
