<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Vessel
 *
 * @property int $id
 * @property string $name
 * @property string $imo_number
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VesselOpex[] $operational_expenses
 * @property-read int|null $operational_expenses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Voyage[] $voyages
 * @property-read int|null $voyages_count
 * @method static \Database\Factories\VesselFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Vessel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vessel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vessel query()
 * @method static \Illuminate\Database\Eloquent\Builder|Vessel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vessel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vessel whereImoNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vessel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vessel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Vessel extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'imo_number'];

    public function voyages()
    {
        return $this->hasMany(Voyage::class);
    }

    public function operational_expenses()
    {
        return $this->hasMany(VesselOpex::class);
    }
}
