<?php

namespace App\Models;

use App\Exceptions\InvalidDataException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * \App\Models\Voyage
 *
 * @property int $id
 * @property int $vessel_id
 * @property string $code
 * @property \Illuminate\Support\Carbon $start
 * @property \Illuminate\Support\Carbon|null $end
 * @property string $status
 * @property float|null $revenues
 * @property float|null $expenses
 * @property float|null $profit
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read double|null $daily_average_net_profit
 * @property-read double|null $daily_average_profit
 * @property-read int $duration
 * @property-read double|null $net_profit
 * @property-read double $total_expenses
 * @property-read \App\Models\Vessel $vessel
 * @method static \Database\Factories\VoyageFactory factory(...$parameters)
 * @method static Builder|Voyage newModelQuery()
 * @method static Builder|Voyage newQuery()
 * @method static Builder|Voyage ongoing()
 * @method static Builder|Voyage query()
 * @method static Builder|Voyage whereCode($value)
 * @method static Builder|Voyage whereCreatedAt($value)
 * @method static Builder|Voyage whereEnd($value)
 * @method static Builder|Voyage whereExpenses($value)
 * @method static Builder|Voyage whereId($value)
 * @method static Builder|Voyage whereProfit($value)
 * @method static Builder|Voyage whereRevenues($value)
 * @method static Builder|Voyage whereStart($value)
 * @method static Builder|Voyage whereStatus($value)
 * @method static Builder|Voyage whereUpdatedAt($value)
 * @method static Builder|Voyage whereVesselId($value)
 * @mixin \Eloquent
 */
class Voyage extends Model
{
    use HasFactory;

    /**
     * Allowed voyage statuses. First status is the default.
     */
    public const STATUSES = ['pending', 'submitted', 'ongoing'];

    protected $fillable = ['vessel_id', 'start', 'end', 'status', 'revenues', 'expenses'];

    protected $dates = ['start', 'end'];
    protected $casts = [
        'revenues' => 'double',
        'expenses' => 'double',
        'profit'   => 'double',
    ];

    protected $attributes = [
        'status' => self::STATUSES[0],
    ];

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOngoing(Builder $query)
    {
        return $query->where('status', 'ongoing');
    }

    /**
     * @return string
     */
    public function calculateCode()
    {
        // I don't think it should validate for filled 'vessel' and 'start'.
        return $this->attributes['code'] = Str::slug($this->vessel->name) . "-" . $this->start->toDateString();
    }

    /**
     * Requires revenues and expenses
     * @return double
     */
    public function calculateProfit()
    {
        // I don't think it should validate for filled 'revenues' and 'expenses'.
        return $this->profit = $this->revenues - $this->expenses;
    }

    /**
     * Mutator for status attribute.
     * Added for system wide validation, not only for rest api.
     * @param string $status
     * @return string
     * @throws InvalidDataException
     */
    public function setStatusAttribute($status)
    {
        throw_unless(in_array($status, static::STATUSES), InvalidDataException::class, "Invalid voyage status '$status'");
        throw_if($status == 'submitted' && (empty($this->start) || empty($this->end) || empty($this->revenues) || empty($this->expenses)), InvalidDataException::class, "Invalid status '$status' for current state of voyage.");
        return $this->attributes['status'] = $status;
    }

    /**
     * Mutator for end attribute.
     * Added for system wide validation, not only for rest api..
     * @param null|mixed $ends_at
     * @return null|Carbon
     * @throws InvalidDataException
     */
    public function setEndAttribute($ends_at)
    {
        if (is_null($ends_at)) {
            return $this->attributes['end'] = null;
        }
        throw_unless($this->start->isBefore($ends_at), InvalidDataException::class, "End date prior to start.");
        return $this->attributes['end'] = $ends_at;
    }

    /**
     * Voyage duration in days. If end is empty then until now.
     * @return int
     */
    public function getDurationAttribute()
    {
        return $this->start->diffInDays($this->end ?? now());
    }

    /**
     * @return double|null
     */
    public function getDailyAverageProfitAttribute()
    {
        // in case it is not submitted
        if (empty($this->profit))
            return null;
        return $this->profit / $this->duration;
    }

    /**
     * Queries db
     * @return double|null
     */
    public function getNetProfitAttribute()
    {
        // in case it is not submitted
        if (empty($this->profit))
            return null;
        return $this->profit - $this->total_expenses;
    }

    /**
     * Queries db
     * @return double
     */
    public function getTotalExpensesAttribute()
    {
        return (double)$this->operationalExpensesQuery()->sum('expenses');
    }

    /**
     * @param null $net_profit
     * @return double|null
     */
    public function getDailyAverageNetProfitAttribute($net_profit = null)
    {
        // in case it is not submitted
        if (empty($this->profit))
            return null;
        // cache/provide net_profit if already calculated so that it does not query db again to recalculate it
        return ($net_profit ?? $this->net_profit) / $this->duration;
    }

    /**
     * @return Builder
     */
    public function operationalExpensesQuery()
    {
        return VesselOpex::where('vessel_id', $this->vessel_id)
            ->whereBetween('date', [$this->start, $this->end ?? now()]);
    }

    public function vessel()
    {
        return $this->belongsTo(Vessel::class);
    }

}
