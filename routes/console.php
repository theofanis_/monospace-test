<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');


Artisan::command('clear:caches', function () {
    $this->call('clear-compiled');
    $this->call('cache:clear');
    $this->call('config:clear');
    $this->call('route:clear');
    $this->call('view:clear');
})->describe('Clear all application caches');

Artisan::command('ide-helper:all', function () {
    rescue(function () {
        $this->call('ide-helper:eloquent');
        $this->call('ide-helper:model', ['--reset' => true, '--write' => true]);
        $this->call('ide-helper:generate');
        $this->call('ide-helper:meta');
    });
})->describe('Call all ide-helper functions.');

