<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('voyages', 'API\VoyageController')->except(['destroy']);

Route::post('/vessels/{vessel}/vessel-opex', 'API\VesselOpexController@store')->name('vessels.opex.store');

Route::get('/vessels/{vessel}/financial-report', 'API\VesselController@financialReport')->name('vessels.financial-report');
