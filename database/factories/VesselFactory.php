<?php

namespace Database\Factories;

use App\Models\Vessel;
use Illuminate\Database\Eloquent\Factories\Factory;

class VesselFactory extends Factory
{
    protected $model = Vessel::class;

    public function definition()
    {
        return [
            'name'       => $this->faker->name,
            'imo_number' => $this->faker->unique()->randomNumber(),
        ];
    }
}
