<?php

namespace Database\Factories;

use App\Models\Vessel;
use App\Models\Voyage;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class VoyageFactory extends Factory
{
    protected $model = Voyage::class;

    public function definition()
    {
        return [
            'vessel_id' => Vessel::query()->inRandomOrder()->first()->id,
            'start'     => $start = $this->faker->dateTimeBetween('-1 year', '+1 year'),
            'end'       => $this->faker->dateTimeBetween((new Carbon($start))->addDay(), '+1 year'),
            'revenues'  => $revenues = $this->faker->numberBetween(1000, 99999999),
            'expenses'  => $this->faker->numberBetween(0, $revenues - 1000),
        ];
    }
}
