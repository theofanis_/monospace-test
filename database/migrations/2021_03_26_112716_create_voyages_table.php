<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoyagesTable extends Migration
{
    public function up()
    {
        Schema::create('voyages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('vessel_id');
            $table->foreign('vessel_id')->references('id')->on('vessels')->onDelete('restrict');
            $table->string('code');
            $table->date('start');
            $table->date('end')->nullable();
            $table->enum('status', \App\Models\Voyage::STATUSES)->default(\App\Models\Voyage::STATUSES[0]);
            $table->double('revenues', 8, 2)->nullable();
            $table->double('expenses', 8, 2)->nullable();
            $table->double('profit', 8, 2)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('voyages');
    }
}
