<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVesselOpexesTable extends Migration
{
    public function up()
    {
        Schema::create('vessel_opex', function (Blueprint $table) {
            $table->id();
            $table->foreignId('vessel_id');
            $table->foreign('vessel_id')->references('id')->on('vessels')->onDelete('restrict');
            $table->date('date');
            $table->double('expenses', 8, 2);
            $table->timestamps();
            $table->unique(['vessel_id', 'date']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('vessel_opex');
    }
}
