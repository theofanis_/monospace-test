<?php

namespace Database\Seeders;

use App\Models\Vessel;
use Illuminate\Database\Seeder;

class VesselSeeder extends Seeder
{
    public function run()
    {
        if (app()->environment('local')) {
            Vessel::factory()->count(10)->create();
        }
    }
}
