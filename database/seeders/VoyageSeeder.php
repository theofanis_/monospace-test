<?php

namespace Database\Seeders;

use App\Models\Voyage;
use Illuminate\Database\Seeder;

class VoyageSeeder extends Seeder
{
    public function run()
    {
        if (app()->environment('local')) {
            Voyage::factory()->count(50)->create();
        }
    }
}
